// no.2.1
db.fruits.aggregate([
    { $unwind: "$origin" },

])

// no.2.2
db.fruits.aggregate([
    {$match: { onSale: true } },
    { $count: "fruitsOnSale"}

])

// no. 3
db.fruits.aggregate([
    {$match: { stock: { $gte: 20} } },
    { $count: "enoughStock"}
])



// no. 4
db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: { _id: "$supplier_id", averagePrice: { $avg: "$price" }} }
])


// no. 5
db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" }} }
])


// no. 6
db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: { _id: "$supplier_id", minPrice: { $min: "$price" }} }
])